======================
Evonove React tutorial
======================

This project provides the React tutorial scaffolding. It's enough that you clone this repository with::

    $ git clone git@gitlab.com:evonove/react-es6-workshop.git

then initialize your own repository::

    $ cd react-es6-workshop
    $ rm -rf .git/
    $ git init
    $ git add .
    $ git commit -m 'initial commit'

Getting started
---------------

The tutorial is distributed with the following structure::

    .
    ├── README.rst
    └── client
        ├── index.html
        ├── js
        │   └── index.js
        └── sass
            └── main.scss

During the workshop, you will update the ``index.html`` and the ``index.js`` files. Everything else
is already provided through ``npm`` dependencies and ``Webpack``.

Launching the server
~~~~~~~~~~~~~~~~~~~~

Webpack offers a builtin server so to start everything you should just::

    $ npm start

What we're doing
----------------

Build React components using Webpack and ES2015 syntax.